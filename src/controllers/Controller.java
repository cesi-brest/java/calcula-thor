package controllers;

import model.Model;
import view.View;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



public class Controller implements ActionListener {
    Model model1;
    View ecran;

    public Controller(Model model, View view){
        this.model1 = model;
        this.ecran = view;
        for (JButton pButton : this.ecran.getButtonsChiffre()) {
            pButton.addActionListener(this);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e){
        JButton source = (JButton)e.getSource();
        String operation = this.model1.getOperation();

        if (operation.isEmpty()) {
            String val1 = this.model1.getValeur1();
            model1.setValeur1(val1 + source.getText());

        } else {

            String val2 = this.model1.getValeur2();
            model1.setValeur2(val2 + source.getText());

        }
        String newValue = this.model1.getValeur1() + this.model1.getOperation() + this.model1.getValeur2();
        this.ecran.getField().setText(newValue);
    }

}
