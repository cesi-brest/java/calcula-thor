package controllers;

import model.Model;
import view.View;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.http.WebSocket;

public class ControllerOperation implements WebSocket.Listener, ActionListener {

    Model model1;
    View ecran;
    //Les adresses du model, il est instancié on le fait qu'une seule fois

    public ControllerOperation(Model model, View view){
        this.model1 = model;
        this.ecran = view;
        for (JButton pButton : this.ecran.getButtonsOperateur()){
            pButton.addActionListener(this);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e){
        JButton source = (JButton)e.getSource();
        model1.setOperation(source.getText());
        String newValue = this.model1.getValeur1() + this.model1.getOperation() + this.model1.getValeur2();
        this.ecran.getField().setText(newValue);
    }
}
