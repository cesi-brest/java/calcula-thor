package controllers;

import model.Model;
import view.View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.http.WebSocket;

public class ControllerEgal implements WebSocket.Listener, ActionListener {

    Model model1;
    View ecran;
    //Les adresses du model, il est instancié on le fait qu'une seule fois

    public ControllerEgal(Model model, View view){
        this.model1 = model;
        this.ecran = view;
        this.ecran.getButtonEgal().addActionListener(this);
    }
    @Override
    public void actionPerformed(ActionEvent e){
        String val1 = this.model1.getValeur1();
        String val2 = this.model1.getValeur2();
        String ope = this.model1.getOperation();

        int nb1 = Integer.valueOf(val1);
        int nb2 = Integer.valueOf(val2);
        String sResult = "";

        if (ope == "+") {
            int nRes = nb1 + nb2;
            sResult = String.format("%d", nRes);
        } else if (ope == "-") {
            int nRes = nb1 - nb2;
            sResult = String.format("%d", nRes);

        } else  if (ope == "*") {
            int nRes = nb1 * nb2;
            sResult = String.format("%d", nRes);
        } else if (ope == "/") {
            int nRes = nb1 / nb2;
            sResult = String.format("%d", nRes);
        }
        this.ecran.getField().setText(sResult);
        this.model1.setValeur1("");
        this.model1.setValeur2("");
        this.model1.setOperation("");

    }
}
