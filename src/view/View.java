package view;
//Par convention les packages sont en minuscules

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class View {
    JFrame frame;
    JTextField field;
    JButton buttonPlus, buttonEgal, buttonMoins, buttonDivise, buttonMultiple;
    List<JButton> l_jButtonChiffre = new ArrayList<JButton>();
    List <JButton> l_jButtonOperateur = new ArrayList<JButton>();

    public List<JButton> getButtonsChiffre() {
        return l_jButtonChiffre;
    }

    public List<JButton> getButtonsOperateur() {
        return l_jButtonOperateur;
    }

    public JButton getButtonEgal() {
        return buttonEgal;
    }

    public JFrame getFrame() {
        return frame;
    }

    public JTextField getField() {
        return field;
    }

    public JPanel init(){
        frame = new JFrame("Calculatrice");


        GridLayout opeLayout = new GridLayout(3,2);
        final JPanel panelOperation = new JPanel();
        panelOperation.setLayout(opeLayout);

        List<String> l_ope = Arrays.asList("+","-", "/","*");

        for (String operation : l_ope){
            JButton pButton = new JButton(operation);
            l_jButtonOperateur.add(pButton);
            panelOperation.add(pButton);
        }

        private Component getPanelChiffres(){
            GridLayout clavierLayout = new GridLayout(4,3);
            final JPanel panelChiffres = new JPanel();
            panelChiffres.setLayout(clavierLayout);
            List<String> l_chiffres = Arrays.asList("7","8","9","4","5","6","1","2","3");
            for(String chiffres : l_chiffres){
                JButton pButton = new JButton(chiffres);
                l_jButtonChiffre.add(pButton);
                panelChiffres.add(pButton);
            }
            return panelChiffres;
        }

        GridLayout layoutCalculatrice = new GridLayout(3,1);
        field = new JTextField();
        field.setColumns(10);
        frame.add(field);
        frame.add(this.getPanelChiffres, BorderLayout.NORTH);
        frame.add(panelOperation, BorderLayout.NORTH);

        buttonEgal = new JButton("=");

        frame.setLayout(new FlowLayout());
        frame.getContentPane().add(field);
        frame.getContentPane().add(buttonEgal);
        frame.setVisible(true);
        frame.pack();
    }
}
