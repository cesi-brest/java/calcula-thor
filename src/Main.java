// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
import controllers.ControllerEgal;
import controllers.ControllerOperation;
import model.Model;
import view.View;
import controllers.Controller;
public class Main {
    public static void main(String[] args) {
        View view = new View();
        view.init();

        Model model = new Model();

        new Controller(model, view);
        new ControllerOperation(model, view);
        new ControllerEgal(model, view);
        //Pas besoin d'instancier lorsqu'on s'en sert pas
        }
    }