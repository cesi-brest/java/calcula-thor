package model;

import java.util.Observable;
public class Model extends Observable {
    String valeur1 = new String();
    String valeur2 = new String();
    String operation = new String();

    public String getValeur1() {
        return valeur1;
    }

    public String getValeur2() {
        return valeur2;
    }

    public String getOperation() {
        return operation;
    }

    public void setValeur1(String valeur) {
        this.valeur1 = valeur;
        System.out.println("Nouvelle valeur : " + valeur);
        //this.setChanged();
        //this.notifyObservers();
    }

    public void setValeur2(String valeur) {
        this.valeur2 = valeur;
        System.out.println("Nouvelle valeur : " + valeur);
        //this.setChanged();
        //this.notifyObservers();
    }

    public void setOperation(String operation) {
        this.operation = operation;
        System.out.println("Opération : " + operation);
        //this.setChanged();
        //this.notifyObservers();
    }

}
